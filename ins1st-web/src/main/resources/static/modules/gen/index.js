layui.use(['form'], function () {
    var o = layui.$,
        form = layui.form;

    form.verify({});

    form.on('submit(add)',
        function (data) {
            var arr = new Array();
            $("input:checkbox[name='gens']:checked").each(function (i) {
                arr[i] = $(this).val();
            });
            for (var i = 0; i < arr.length; i++) {
                var choose = arr[i];
                switch (choose) {
                    case "1":
                        data.field.genController = true;
                        break;
                    case "2":
                        data.field.genService = true;
                        break;
                    case "3":
                        data.field.genServiceImpl = true;
                        break;
                    case "4":
                        data.field.genEntity = true;
                        break;
                    case "5":
                        data.field.genDao = true;
                        break;
                    case "6":
                        data.field.genXml = true;
                        break;
                    case "7":
                        data.field.genIndexHtml = true;
                        break;
                    case "8":
                        data.field.genAddHtml = true;
                        break;
                    case "9":
                        data.field.genEditHtml = true;
                        break;
                    case "10":
                        data.field.genIndexJs = true;
                        break;
                    case "11":
                        data.field.genAddJs = true;
                        break;
                    case "12":
                        data.field.genEditJs = true;
                        break;
                }
            }
            $.ajax({
                url: Base.ctxPath + "/gen/gen",
                type: "post",
                data: data.field,
                success: function (result) {
                    if (result.success) {
                        Base.alert(result.message);
                    } else {
                        Base.fail(result.message);
                    }
                }
            });
            return false;
        });
});